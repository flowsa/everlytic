<?php
/**
 * everlytic  plugin for Craft CMS 3.x
 *
 * Posts form submissions to Everlytic
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

namespace flowsaeverlytic\everlytic\variables;
use flowsaeverlytic\everlytic\services\EverlyticService as EverlyticServiceService;

use flowsaeverlytic\everlytic\Everlytic;

use Craft;

/**
 * everlytic  Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.everlytic }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    Flow
 * @package   Everlytic
 * @since     1.0.0
 */
class EverlyticVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.everlytic.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.everlytic.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function savePost($entry)
    {
          (new EverlyticServiceService())->sendToEverlytic($entry);
    }
}
