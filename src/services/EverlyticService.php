<?php
/**
 * everlytic  plugin for Craft CMS 3.x
 *
 * Posts form submissions to Everlytic
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

namespace flowsaeverlytic\everlytic\services;

use flowsaeverlytic\everlytic\Everlytic;

use Craft;
use craft\base\Component;

/**
 * EverlyticService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Flow
 * @package   Everlytic
 * @since     1.0.0
 */
class EverlyticService extends Component
{
    // Public Methods
    // =========================================================================

    /**
     * This function can literally be anything you want, and you can have as many service
     * functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     Everlytic::$plugin->everlyticService->exampleService()
     *
     * @return mixed
     */
    public function sendToEverlytic($entry)
    {   
        $fieldArray = [];
        $fieldArray['emailAddress'] = "contact_email";
        $fieldArray['firstName'] = "contact_name";
        $fieldArray['lastName'] = "contact_lastname";
        $fieldArray['message'] = "comments_or_questions52";
        $fieldArray['companyName'] = "contact_company_name";
        $fieldArray['contactNumber'] = "contact_number";
        $fieldArray['jobTitle'] = "contact_company_position";
        $fieldArray['country'] = "contact_country";
        $fieldArray['dietaryRequirements'] = "cfv_dietary_requirements49";
        $fieldArray['howCanWeHelp'] = "howCanWeHelp";
        $fieldArray['howDoYouCurrentlyManageYourRisk'] = "cfv_how_do_you_currently_manage_your_risk73";
        $fieldArray['howManyEmployeesWorkAtYourCompany'] = "cfv_how_many_employees_work_there44";
        $fieldArray['pageVisited'] = "cfv_page_visited49";
        $fieldArray['pleaseCheckYesToConfirmYourAttendance'] = "cfv_confirm_attendance89";
        $fieldArray['whereDidYouHearAboutUs'] = "cfv_where_did_you_hear_about_us21";
        $fieldArray['solutions'] = "cfv_solution94";
        $fieldArray['marketing'] = "cfv_id_like_to_receive_marketing_communications5";
        $fieldArray['demo'] = "cfv_id_like_to_see_a_demo_of_isometrix37";
        
        
        
        $json = '{';
        foreach ($entry->getFieldLayout()->getFields() as $fieldLayoutField)
        {
            if($fieldLayoutField->className() != "flowsa\flowrecaptchav3\fields\FlowRecaptchaV3Field" && $fieldLayoutField->className() != "craft\\fields\\Assets"){
                if( $fieldLayoutField['handle'] == 'everlyticlistId'){
                    $everlyticListId = $entry[$fieldLayoutField['handle']];
                }

                if (array_key_exists($fieldLayoutField['handle'], $fieldArray)) {
                    if(is_object($entry[$fieldLayoutField['handle']])){
                        $label = null;
                        foreach($entry[$fieldLayoutField['handle']] as $key => $value){
                            if($key === 'label'){
                                $label = $value;
                            }
                        }
                        $json = $json.'"'.$fieldArray[$fieldLayoutField['handle']].'":"'. $label.'", ';
                    } else {
                        $json = $json.'"'.$fieldArray[$fieldLayoutField['handle']].'":"'.$entry[$fieldLayoutField['handle']].'", ';
                    }
                }
            }
        }
        $json = $json . '"on_duplicate":"update","create_notifications":"yes","list_id":{"'.$everlyticListId.'":"subscribed"}}';
        $username = "Fabio.De.Abreu";
        $url = 'http://isometrix.everlytic.net/api/2.0/contacts';
        $api_key = "5d6S5RqWBcpRAtm1qPEpRJMXtVF1UWAY_9";

        $method = 'POST';
        $cSession = curl_init();
        $headers = array();
        $auth = base64_encode($username . ':' . $api_key);
        $headers[] = 'Authorization: Basic ' . $auth;
        curl_setopt($cSession, CURLOPT_URL, $url);
        curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cSession, CURLOPT_HEADER, false);
        curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, strtoupper($method));
        curl_setopt($cSession, CURLOPT_POSTFIELDS, $json);
        $headers[] = 'Content-Type: application/json';
        curl_setopt($cSession, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($cSession, CURLOPT_VERBOSE, 1);
        curl_setopt($cSession, CURLOPT_HEADER, 1);
        $result = curl_exec($cSession);
        curl_close($cSession);
        return $result;
    }
}
