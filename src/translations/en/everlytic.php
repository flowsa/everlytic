<?php
/**
 * everlytic  plugin for Craft CMS 3.x
 *
 * Posts form submissions to Everlytic
 *
 * @link      www.flowsa.com
 * @copyright Copyright (c) 2019 Flow
 */

/**
 * everlytic  en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('everlytic', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Flow
 * @package   Everlytic
 * @since     1.0.0
 */
return [
    'everlytic  plugin loaded' => 'everlytic  plugin loaded',
];
