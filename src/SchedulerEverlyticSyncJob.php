<?php

namespace flowsaeverlytic\everlytic;

/**
 * SchedulerEverlyticSyncJob 
 *
 * This Job will send an element to everlytic
 *
 *
 * @package   Scheduler
 * @copyright Copyright (c) 2018, Supercool Ltd
 * @link      https://github.com/supercool/Scheduler
 */

use Craft;

use supercool\scheduler\jobs\BaseSchedulerJob;
use flowsaeverlytic\everlytic\services\EverlyticService as EverlyticServiceService;


class SchedulerEverlyticSyncJob extends BaseSchedulerJob
{

	// Public Methods
	// =========================================================================

	/**
	 * @inheritDoc IScheduler_Job::run()
	 *
	 * @return bool
	 */
	public function run()
	{
		// Get the model
		$job = $this->model;

		// Get the elementId from the model settings

		$entry = $job->settings['entry'];

        try
        {
			$element = Craft::$app->elements->getElementById($entry['id']);
			$result = (new EverlyticServiceService())->sendToEverlytic($element);

        } 
		catch (\Exception $e)
		{
			Craft::error(Craft::t('scheduler', 'An exception was thrown while trying to send the element with the ID “'.$element->id.'”: '.$e->getMessage()));
			return false;
		}

		return true;
	}

}


